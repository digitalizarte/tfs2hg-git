﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tfs2Hg
{
    public class CommandLineArguments
    {
        public bool Parsed { get; private set; }
        private readonly ILookup<String, String> arguments;

        public IEnumerable<String> this[string key]
        {
            get { return arguments[key]; }
        }

        public bool Contains(string key)
        {
            return arguments.Contains(key);
        }

        public CommandLineArguments(IEnumerable<String> args)
        {
            try
            {
                var argValues = from x in args
                                let parts = x.Split('=')
                                select new
                                {
                                    Argument = parts[0],
                                    Value = parts[1]
                                };

                arguments = argValues.ToLookup(x => x.Argument, x => x.Value);

                Parsed = true;
            }
            catch (Exception)
            {
                return;
            }
        }

        public override string ToString()
        {
            StringBuilder toStringBuilder = new StringBuilder();
            foreach (var argument in arguments)
            {
                foreach (var argumentValue in arguments[argument.Key])
                {
                    toStringBuilder.AppendLine(String.Format("{0} = {1}", argument.Key, argumentValue));
                }
            }
            return toStringBuilder.ToString();
        }

        public bool ContainsAll(params string[] args)
        {
            return args.All(argument => arguments.Contains(argument));
        }
    }
}