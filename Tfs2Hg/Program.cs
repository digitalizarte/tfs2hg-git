﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using Microsoft.TeamFoundation.VersionControl.Client;

namespace Tfs2Hg
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1 && args[0] == "--license")
            {
                DisplayLicense();
                return;
            }

            CommandLineArguments commandLineArgs = new CommandLineArguments(args);
  
            string tfsUriString = commandLineArgs["--tfsUri"].FirstOrDefault();
            string repositoryPath = commandLineArgs["--repositoryPath"].FirstOrDefault();
            string user = commandLineArgs["--user"].FirstOrDefault();
            string password = commandLineArgs["--password"].FirstOrDefault();
            string hgCommand = commandLineArgs["--hgPath"].FirstOrDefault();

            if (!commandLineArgs.Parsed
                || !Uri.IsWellFormedUriString(tfsUriString, UriKind.Absolute)
                || String.IsNullOrEmpty(repositoryPath)
                || String.IsNullOrEmpty(user) && !String.IsNullOrEmpty(password)
                || !String.IsNullOrEmpty(user) && String.IsNullOrEmpty(password)
                )
            {
                Console.WriteLine(Usage.Short);
                Environment.Exit(255);
            }

            Uri tfsUri = new Uri(tfsUriString);
            NetworkCredential credentials = (user != null) ? new NetworkCredential(user, password) : null;
        
            try
            {
                if (!Directory.Exists(repositoryPath))
                {
                    Directory.CreateDirectory(repositoryPath);
                }

                HgUtility hgUtility = String.IsNullOrEmpty(hgCommand) ? new HgUtility(repositoryPath) : new HgUtility(repositoryPath, hgCommand);
                TfsUtility tfsUtility = new TfsUtility(repositoryPath, tfsUri, credentials);

                if (!hgUtility.IsRepository())
                {
                    int returnCode = hgUtility.Initialize();
                    if (returnCode != 0)
                        Environment.Exit(returnCode);
                }

                hgUtility.UpdateHgIgnore(".lastTfsChangeSet");

                FileInfo lastTfsChangeSetFile = new FileInfo(Path.Combine(repositoryPath, ".lastTfsChangeSet"));

                ChangesetVersionSpec firstUnfectchedChangesetVersion = GetFromVersionSpec(lastTfsChangeSetFile);

                Console.WriteLine("Getting history - this might take a couple of seconds...");
                var lastChangeSetInTfs = tfsUtility.GetChangesetHistory(VersionSpec.Latest, VersionSpec.Latest).SingleOrDefault();

                if (lastChangeSetInTfs != null && lastChangeSetInTfs.ChangesetId < firstUnfectchedChangesetVersion.ChangesetId)
                {
                    Console.WriteLine("No newer changesets available");
                    return;
                }

                var changeSets = tfsUtility.GetChangesetHistory(firstUnfectchedChangesetVersion, null);

                foreach (Changeset changeset in changeSets)
                {
                    Console.WriteLine("Getting changeset {0} ({1}) by {2} on {3}", changeset.ChangesetId, changeset.Comment, changeset.Committer, changeset.CreationDate);
                    
                    tfsUtility.GetChangeset(changeset);

                    int returnCode = hgUtility.CommitToHg(changeset);

                    Console.WriteLine("-------------------------------------");

                    if (returnCode == 0)
                    {
                        // Write latest changeset version to file
                        File.WriteAllText(lastTfsChangeSetFile.FullName, changeset.ChangesetId.ToString());
                    }
                    else
                    {
                        Environment.Exit(returnCode);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An exception occurred: {0}", ex);
                Environment.Exit(255);
            }
        }

        private static void DisplayLicense()
        {
            Stream resourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Tfs2Hg.LICENSE.txt");
            if (resourceStream != null)
            {
                using (StreamReader streamReader = new StreamReader(resourceStream))
                {
                    string license = streamReader.ReadToEnd();
                    Console.WriteLine(license);
                }
            }
        }

        private static ChangesetVersionSpec GetFromVersionSpec(FileInfo lastTfsChangesetFile)
        {
            int lastFetchedChangeset = lastTfsChangesetFile.Exists ? Int32.Parse(File.ReadAllText(lastTfsChangesetFile.FullName).Trim()) : 0;

            return new ChangesetVersionSpec(lastFetchedChangeset + 1);
        }
    }
}
