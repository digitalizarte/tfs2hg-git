﻿using System;
using System.Diagnostics;

namespace Tfs2Hg
{
    public static class Tools
    {
        public static int ExecuteCommand(string fileName, string arguments, out string output)
        {
            Process p = new Process
                            {
                                StartInfo = {UseShellExecute = false, RedirectStandardOutput = true, FileName = fileName, Arguments = arguments}
                            };
            p.Start();
            output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();

            return p.ExitCode;
        }
        
        /// <summary>
        /// method for converting a System.DateTime value to a UNIX Timestamp
        /// </summary>
        /// <param name="value">date to convert</param>
        /// <returns></returns>
        public static double ToTimestamp(this DateTime value)
        {
            //create Timespan by subtracting the value provided from
            //the Unix Epoch
            TimeSpan span = (value.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, 0));

            //return the total seconds (which is a UNIX timestamp)
            return span.TotalSeconds;
        }
    }
}