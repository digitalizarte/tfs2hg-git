﻿namespace Tfs2Hg
{
    public static class Usage
    {
        public static string Short
        {
            get
            {
                return
                    @"Tfs2Hg - Get all changesets from a TFS project and commit them to a Mercurial repository, one by one.
If missing, the repository folder is created, mapped to a new workspace and a Mercurial repository is initialized.

Usage:
Tfs2Hg --tfsUri=""URI of the project in TFS"" --repositoryPath=""path to local repository folder"" [--hgPath=""path to hg.exe""] [--user=""TFS user name"" --password=""TFS password""]
Tfs2Hg --license";
            }
        }
    }
}
